//
// Copyright (c) 2015, Adnan Ademovic
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "localization.h"

#include <algorithm>
#include <chrono>
#include <cmath>
#include <random>

#include "ros/console.h"

namespace com {
namespace ademovic {
namespace localization {
namespace {

constexpr double pi() {
  return atan(1) * 4.0;
}

double DistanceToQuality(double distance) {
  return 10000.0 / (distance + 0.5);
}

}  // namespace

Localization::Localization(int particles, double eps, int particle_to_hit_ratio,
                           int filter_intertia, const std::string& frame_id,
                           const std::string& map_frame_id)
    : default_particle_count_(particles), particle_count_(particles),
      eps_(eps), particle_to_hit_ratio_(particle_to_hit_ratio),
      filter_intertia_(filter_intertia), frame_id_(frame_id),
      map_frame_id_(map_frame_id), map_(nullptr),
      solutions_(new std::vector<std::unique_ptr<Solution>>) {}

void Localization::LoadMap(const nav_msgs::OccupancyGrid& occupancy_grid) {
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  map_.reset(new Map(occupancy_grid));
  std::default_random_engine re(seed);
  theta_ = std::uniform_real_distribution<double>(-pi(), pi());
  for (int i = 0; i < particle_count_; ++i) {
    std::pair<double, double> p(map_->GetRandomPoint(&re));
    solutions_->emplace_back(new Solution(p.first, p.second, theta_(re),
                                          0.0, 0.0));
  }
}

void Localization::Move(
    double linear_velocity, double angular_velocity, double delta_time) {
  for (std::unique_ptr<Solution>& s : *solutions_)
    s->Move(linear_velocity, angular_velocity, delta_time);
}

void Localization::Scan(const sensor_msgs::LaserScan& data, double delta_time) {
  if (map_.get() == nullptr)
    return;
  std::vector<double> qualities;
  std::unique_ptr<std::vector<std::unique_ptr<Solution> > > new_solutions(
      new std::vector<std::unique_ptr<Solution>>);
  for (const std::unique_ptr<Solution>& s : *solutions_)
    qualities.push_back(DistanceToQuality(s->Scan(*map_, data)));

  // Our goal is to keep around 1 in ? of particles to be within a certain
  // error. The upper limit is the value of the default_particle_count set in
  // the constructor, and the lower limit should be 10% of that.
  double allowed_quality = DistanceToQuality(eps_);

  int good_particles(0);
  for (double d : qualities)
    if (d > allowed_quality)
      ++good_particles;

  ROS_INFO("There have been %d particles, %d with low error.",
      particle_count_, good_particles);

  good_particles *= particle_to_hit_ratio_;

  int goal_particle_count(default_particle_count_);

  if (good_particles)
    goal_particle_count = (int)(
        ((int64_t)particle_count_ * (int64_t)particle_count_) / good_particles);
  if (goal_particle_count > default_particle_count_)
    goal_particle_count = default_particle_count_;
  if (goal_particle_count < default_particle_count_ / 10)
    goal_particle_count = default_particle_count_ / 10;

  // Use some inertia in going for the goal particle count
  goal_particle_count =
      (particle_count_ * filter_intertia_ + goal_particle_count) /
      (filter_intertia_ + 1);

  if (goal_particle_count > default_particle_count_)
    goal_particle_count = default_particle_count_;
  if (goal_particle_count < default_particle_count_ / 10)
    goal_particle_count = default_particle_count_ / 10;

  std::vector<int> choices(particle_count_, 0);
  size_t best_index = std::max_element(qualities.begin(), qualities.end()) -
                      qualities.begin();
  ++choices[best_index];

  best_estimate_ = nav_msgs::Odometry();
  best_estimate_.header.frame_id = map_frame_id_;
  best_estimate_.pose.pose.position.x = (*solutions_)[best_index]->Point().x;
  best_estimate_.pose.pose.position.y = (*solutions_)[best_index]->Point().y;
  double orientation = (*solutions_)[best_index]->Orientation();
  best_estimate_.pose.pose.orientation.z = sin(orientation / 2.0);
  best_estimate_.pose.pose.orientation.w = cos(orientation / 2.0);

  for (int i = 1; i < particle_count_; ++i)
    qualities[i] += qualities[i - 1];

  std::uniform_real_distribution<double> unif(
      0.0, qualities[particle_count_ - 1]);

  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine re(seed);

  for (int i = 1; i < goal_particle_count / 2; ++i)
    ++choices[std::lower_bound(qualities.begin(), qualities.end(), unif(re)) -
              qualities.begin()];
  for (int i = goal_particle_count / 2; i < goal_particle_count; ++i) {
    std::pair<double, double> p(map_->GetRandomPoint(&re));
    new_solutions->emplace_back(new Solution(
        p.first, p.second, theta_(re), 0.0, 0.0));
  }

  std::normal_distribution<double> position_offset(0.0, 0.2 * delta_time);
  std::normal_distribution<double> orientation_offset(0.0, 0.02 * delta_time);
  std::normal_distribution<double> linvel_offset(0.0, 0.025);
  std::normal_distribution<double> angvel_offset(0.0, pi() * 0.005);

  for (int i = 0; i < particle_count_; ++i)
    if (choices[i]) {
      new_solutions->emplace_back(
          new Solution(*(*solutions_)[i], 0.0, 0.0, 0.0, 0.0, 0.0));
      for (int j = 1; j < choices[i]; ++j) {
        double px, py;
        px = position_offset(re);
        py = position_offset(re);
        new_solutions->emplace_back(new Solution(
            *(*solutions_)[i], px, py, orientation_offset(re),
            linvel_offset(re), angvel_offset(re)));
      }
    }
  new_solutions.swap(solutions_);
  particle_count_ = goal_particle_count;
}

sensor_msgs::PointCloud Localization::SolutionPoints() {
  sensor_msgs::PointCloud retval;
  retval.header.frame_id = map_frame_id_;
  for (std::unique_ptr<Solution>& s : *solutions_)
    retval.points.push_back(s->Point());
  return retval;
}

nav_msgs::Odometry Localization::BestEstimate() {
  return best_estimate_;
}

}  // namespace localization
}  // namespace ademovic
}  // namespace com
