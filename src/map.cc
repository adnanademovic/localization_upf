//
// Copyright (c) 2015, Adnan Ademovic
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "map.h"

#include <algorithm>
#include <cmath>
#include <limits>

namespace com {
namespace ademovic {
namespace localization {
namespace {

double Distance(int dx, int dy) {
  return double(dx * dx + dy * dy);
}

std::vector<std::vector<bool> > ApplySobelWithThreshold(
    const std::vector<std::vector<bool> >& data) {
  size_t len_x = data.size();
  size_t len_y = data[0].size();
  std::vector<std::vector<bool> > edge(len_x, std::vector<bool>(len_y, true));
  for (size_t x = 1; x < len_x - 1; ++x)
    for (size_t y = 1; y < len_y - 1; ++y) {
      int k_x = (data[x - 1][y - 1] ? -1 : 0) +
                (data[x - 1][y    ] ? -2 : 0) +
                (data[x - 1][y + 1] ? -1 : 0) +
                (data[x + 1][y - 1] ?  1 : 0) +
                (data[x + 1][y    ] ?  2 : 0) +
                (data[x + 1][y + 1] ?  1 : 0);
      int k_y = (data[x - 1][y - 1] ? -1 : 0) +
                (data[x    ][y - 1] ? -2 : 0) +
                (data[x + 1][y - 1] ? -1 : 0) +
                (data[x - 1][y + 1] ?  1 : 0) +
                (data[x    ][y + 1] ?  2 : 0) +
                (data[x + 1][y + 1] ?  1 : 0);
      edge[x][y] = (k_x * k_x + k_y * k_y) > 0;
    }
  return edge;
}

}  // namespace

Map::Map(const nav_msgs::OccupancyGrid& oc_grid) {
  x1_ = oc_grid.info.origin.position.x;
  y1_ = oc_grid.info.origin.position.y;
  l_x_ = oc_grid.info.width;
  l_y_ = oc_grid.info.height;
  delta_ = oc_grid.info.resolution;
  distances_.reset(new std::vector<std::vector<std::pair<int, int> > >(
      l_x_ + 2, std::vector<std::pair<int, int> >(
          l_y_ + 2, std::pair<int, int>(-1, -1))));
  occupied_.reset(new std::vector<std::vector<bool> >(
      l_x_, std::vector<bool>(l_y_, false)));
  ok_points_.reset(new std::vector<std::pair<int, int> >);
  for (int x = 0; x < l_x_ + 2; ++x)
    (*distances_)[x][0].first = (*distances_)[x][0].second =
        (*distances_)[x][l_y_ + 1].first = (*distances_)[x][l_y_ + 1].second =
            0;
  for (int y = 0; y < l_y_ + 2; ++y)
    (*distances_)[0][y].first = (*distances_)[0][y].second =
        (*distances_)[l_x_ + 1][y].first = (*distances_)[l_x_ + 1][y].second =
            0;
  std::vector<std::vector<bool> > data(l_x_, std::vector<bool>(l_y_));
  for (int x = 0; x < l_x_; ++x)
    for (int y = 0; y < l_y_; ++y) {
      (*occupied_)[x][y] = data[x][y] = (oc_grid.data[y * l_x_ + x] != 0);
      if (!data[x][y])
        ok_points_->emplace_back(x, y);
    }
  data = ApplySobelWithThreshold(data);

  pos_delta_ = std::uniform_real_distribution<double>(0.0, delta_);
  point_index_ = std::uniform_int_distribution<int>(0, (int)ok_points_->size());

  for (int x = 1; x <= l_x_; ++x)
    for (int y = 1; y <= l_y_; ++y) {
      if (data[x - 1][y - 1]) {
        (*distances_)[x][y].first = (*distances_)[x][y].second = 0;
        continue;
      }
      int horizontal = Distance(
          (*distances_)[x - 1][y].first + 1, (*distances_)[x - 1][y].second);
      int vertical = Distance(
          (*distances_)[x][y - 1].first, (*distances_)[x][y - 1].second + 1);
      if (horizontal < vertical) {
        (*distances_)[x][y].first = (*distances_)[x - 1][y].first + 1;
        (*distances_)[x][y].second = (*distances_)[x - 1][y].second;
      } else {
        (*distances_)[x][y].first = (*distances_)[x][y - 1].first;
        (*distances_)[x][y].second = (*distances_)[x][y - 1].second + 1;
      }
    }
  for (int x = l_x_; x > 0; --x)
    for (int y = l_y_; y > 0; --y) {
      if (data[x - 1][y - 1]) {
        (*distances_)[x][y].first = (*distances_)[x][y].second = 0;
        continue;
      }
      int current = Distance(
          (*distances_)[x][y].first, (*distances_)[x][y].second);
      int horizontal = Distance(
          (*distances_)[x + 1][y].first + 1, (*distances_)[x + 1][y].second);
      int vertical = Distance(
          (*distances_)[x][y + 1].first, (*distances_)[x][y + 1].second + 1);
      if (current < horizontal && current < vertical)
        continue;
      if (horizontal < vertical) {
        (*distances_)[x][y].first = (*distances_)[x + 1][y].first + 1;
        (*distances_)[x][y].second = (*distances_)[x + 1][y].second;
      } else {
        (*distances_)[x][y].first = (*distances_)[x][y + 1].first;
        (*distances_)[x][y].second = (*distances_)[x][y + 1].second + 1;
      }
    }
}

double Map::GetError(double x, double y) const {
  int px = static_cast<int>((x - x1_) / delta_) + 1;
  double infinity = std::numeric_limits<double>::infinity();
  if (px < 0)
    return infinity;
  if (px > l_x_ + 1)
    return infinity;
  int py = static_cast<int>((y - y1_) / delta_) + 1;
  if (py < 0)
    return infinity;
  if (py > l_y_ + 1)
    return infinity;
  return Distance((*distances_)[px][py].first, (*distances_)[px][py].second);
}

bool Map::IsOccupied(double x, double y) const {
  int px = static_cast<int>((x - x1_) / delta_) + 1;
  if (px < 0)
    return true;
  if (px >= l_x_)
    return true;
  int py = static_cast<int>((y - y1_) / delta_) + 1;
  if (py < 0)
    return true;
  if (py >= l_y_)
    return true;
  return (*occupied_)[px][py];
}

std::pair<double, double> Map::GetRandomPoint(
    std::default_random_engine* re) {
  const std::pair<int, int>& initp((*ok_points_)[point_index_(*re)]);
  return {initp.first * delta_ + pos_delta_(*re) + x1_,
          initp.second * delta_ + pos_delta_(*re) + y1_};
}

}  // namespace localization
}  // namespace ademovic
}  // namespace com
