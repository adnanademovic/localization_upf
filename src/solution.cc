//
// Copyright (c) 2015, Adnan Ademovic
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "solution.h"

#include <algorithm>
#include <limits>

namespace com {
namespace ademovic {
namespace localization {

Solution::Solution(
    const Solution& s, double pos_x_offset, double pos_y_offset,
    double orientation_offset, double linvel_offset, double angvel_offset)
    : Solution(s.Point().x + pos_x_offset, s.Point().y + pos_y_offset,
               s.Orientation() + orientation_offset,
               linvel_offset, angvel_offset) {}

Solution::Solution(double pos_x, double pos_y, double angle,
                   double linvel_offset, double angvel_offset)
    : x_(pos_x), y_(pos_y), angle_(angle),
      linvel_offset_(linvel_offset), angvel_offset_(angvel_offset) {}

void Solution::Move(
    double linear_velocity, double angular_velocity, double delta_time) {
  linear_velocity += linvel_offset_;
  angular_velocity += angvel_offset_;
  x_ += linear_velocity * cos(angle_) * delta_time;
  y_ += linear_velocity * sin(angle_) * delta_time;
  angle_ += angular_velocity * delta_time;
}

double Solution::Scan(const Map& map, const sensor_msgs::LaserScan& data) {
  std::vector<double> scan_points;
  size_t scans_count = data.ranges.size();
  double angle = data.angle_min;
  size_t step = 1;
  double cth = cos(angle_);
  double sth = sin(angle_);

  double errors = 0.0;
  size_t successful_scans = 0;
  for (size_t i = 0; i < scans_count;
       angle += data.angle_increment * step, i += step) {
    if (!std::isnormal(data.ranges[i]))
      continue;
    if (data.ranges[i] > data.range_max || data.ranges[i] < data.range_min)
      continue;
    // 0.225 - laser scanner X offset
    errors += map.GetError(
        x_ + cos(angle + angle_) * data.ranges[i] + cth * 0.225,
        y_ + sin(angle + angle_) * data.ranges[i] + sth * 0.225);
    ++successful_scans;
  }
  // Require at least 25% of measurements to be within limits
  if (successful_scans * 4 < scans_count)
    return std::numeric_limits<double>::infinity();
  return errors / successful_scans;
}

geometry_msgs::Point32 Solution::Point() const {
  geometry_msgs::Point32 errors;
  errors.x = x_;
  errors.y = y_;
  return errors;
}

double Solution::Orientation() const {
  return angle_;
}

}  // namespace localization
}  // namespace ademovic
}  // namespace com
