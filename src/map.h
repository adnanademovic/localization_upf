//
// Copyright (c) 2015, Adnan Ademovic
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef COM_ADEMOVIC_LOCALIZATION_POINT_INDEX_H_
#define COM_ADEMOVIC_LOCALIZATION_POINT_INDEX_H_

#include <memory>
#include <utility>
#include <vector>

#include "nav_msgs/OccupancyGrid.h"

namespace com {
namespace ademovic {
namespace localization {

class Map {
 public:
  Map(const nav_msgs::OccupancyGrid& oc_grid);
  virtual ~Map() {}

  double GetError(double x, double y) const;
  bool IsOccupied(double x, double y) const;

  std::pair<double, double> GetRandomPoint(
      std::default_random_engine* re);

 private:
  double x1_, y1_, delta_;
  int l_x_, l_y_;
  std::unique_ptr<std::vector<std::vector<std::pair<int, int> > > > distances_;
  std::unique_ptr<std::vector<std::vector<bool> > > occupied_;
  std::unique_ptr<std::vector<std::pair<int, int> > > ok_points_;
  std::uniform_real_distribution<double> pos_delta_;
  std::uniform_int_distribution<int> point_index_;
};

}  // namespace localization
}  // namespace ademovic
}  // namespace com

#endif  // COM_ADEMOVIC_LOCALIZATION_POINT_INDEX_H_
