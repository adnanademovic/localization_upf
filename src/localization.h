//
// Copyright (c) 2015, Adnan Ademovic
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef COM_ADEMOVIC_LOCALIZATION_LOCALIZATION_H_
#define COM_ADEMOVIC_LOCALIZATION_LOCALIZATION_H_

#include <memory>
#include <vector>

#include "nav_msgs/Odometry.h"
#include "sensor_msgs/LaserScan.h"
#include "sensor_msgs/PointCloud.h"

#include "map.h"
#include "solution.h"

namespace com {
namespace ademovic {
namespace localization {

class Localization {
 public:
  Localization(int particles, double eps, int particle_to_hit_ratio,
               int filter_intertia, const std::string& frame_id,
               const std::string& map_frame_id);
  void LoadMap(const nav_msgs::OccupancyGrid& occupancy_grid);
  void Move(double linear_velocity, double angular_velocity, double delta_time);
  void Scan(const sensor_msgs::LaserScan& data, double delta_time);
  sensor_msgs::PointCloud SolutionPoints();
  nav_msgs::Odometry BestEstimate();
 private:
  int default_particle_count_;
  int particle_count_;
  double eps_;
  int particle_to_hit_ratio_;
  int filter_intertia_;
  std::string frame_id_;
  std::string map_frame_id_;
  std::unique_ptr<Map> map_;
  std::unique_ptr<std::vector<std::unique_ptr<Solution> > > solutions_;
  std::uniform_real_distribution<double> theta_;
  nav_msgs::Odometry best_estimate_;
};

}  // namespace localization
}  // namespace ademovic
}  // namespace com

#endif  // COM_ADEMOVIC_LOCALIZATION_LOCALIZATION_H_
