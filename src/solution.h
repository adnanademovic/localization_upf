//
// Copyright (c) 2015, Adnan Ademovic
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef COM_ADEMOVIC_LOCALIZATION_SOLUTION_H_
#define COM_ADEMOVIC_LOCALIZATION_SOLUTION_H_

#include "geometry_msgs/Point32.h"
#include "sensor_msgs/LaserScan.h"

#include "map.h"

namespace com {
namespace ademovic {
namespace localization {

class Solution {
 public:
  Solution(const Solution& s, double pos_x_offset, double pos_y_offset,
           double orientation_offset, double linvel_offset,
           double angvel_offset);
  Solution(double pos_x, double pos_y, double angle,
           double linvel_offset, double angvel_offset);
  void Move(double linear_velocity, double angular_velocity, double delta_time);
  double Scan(const Map& map, const sensor_msgs::LaserScan& data);
  geometry_msgs::Point32 Point() const;
  double Orientation() const;
 private:
  double x_, y_, angle_, ang_c, ang_s;
  double linvel_offset_, angvel_offset_;
};

}  // namespace localization
}  // namespace ademovic
}  // namespace com

#endif  // COM_ADEMOVIC_LOCALIZATION_SOLUTION_H_
