//
// Copyright (c) 2015, Adnan Ademovic
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "nav_msgs/OccupancyGrid.h"
#include "nav_msgs/Odometry.h"
#include "ros/ros.h"
#include "sensor_msgs/LaserScan.h"
#include "sensor_msgs/PointCloud.h"
#include "tf/transform_broadcaster.h"

#include "localization.h"

com::ademovic::localization::Localization* localization;

bool publishing_flag = false;
double last_move = -1.0;
double last_scan = -1.0;

void OdomCallback(const nav_msgs::Odometry::ConstPtr& msg) {
  double new_time = msg->header.stamp.toSec();
  if (last_move < 0.0)
    last_move = new_time;
  localization->Move(msg->twist.twist.linear.x, msg->twist.twist.angular.z,
                    new_time - last_move);
  last_move = new_time;
}

void BaseScanCallback(const sensor_msgs::LaserScan::ConstPtr& msg) {
  double new_time = msg->header.stamp.toSec();
  if (last_scan < 0.0)
    last_scan = new_time;
  publishing_flag = true;
  localization->Scan(*msg, new_time - last_scan);
  last_scan = new_time;
}

void MapCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg) {
  localization->LoadMap(*msg);
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "localization");
  ros::NodeHandle n;
  ros::NodeHandle params("~");

  std::string odometry_topic, laser_scan_topic, map_source;
  std::string candidates, best_estimate, frame_id, map_frame_id;
  int particles, filter_inertia, particle_to_hit_ratio;
  double error_tolerance;

  params.param<std::string>("odometry", odometry_topic, "odom");
  params.param<std::string>("laserscan", laser_scan_topic, "base_scan");
  params.param<std::string>("map", map_source, "map");
  params.param<std::string>("candidate_cloud", candidates, "candidates");
  params.param<std::string>(
      "estimated_position", best_estimate, "best_estimate");
  params.param<std::string>("frame_id", frame_id, "odom");
  params.param<std::string>("map_frame_id", map_frame_id, "map");
  params.param<int>("particles", particles, 2000);
  params.param<int>("filter_inertia", filter_inertia, 9);
  params.param<int>("particle_to_hit_ratio", particle_to_hit_ratio, 3);
  params.param<double>("error_tolerance", error_tolerance, 0.4);

  com::ademovic::localization::Localization loc(
      particles, error_tolerance, particle_to_hit_ratio, filter_inertia,
      frame_id, map_frame_id);
  localization = &loc;

  tf::TransformBroadcaster tb;
  ros::Subscriber odom_sub = n.subscribe(odometry_topic, 1000, OdomCallback);
  ros::Subscriber base_scan_sub =
      n.subscribe(laser_scan_topic, 1, BaseScanCallback);
  ros::Subscriber map_sub = n.subscribe(map_source, 1000, MapCallback);
  ros::Publisher candidates_pub = n.advertise<sensor_msgs::PointCloud>(
      candidates, 1000);
  ros::Publisher best_estimate_pub = n.advertise<nav_msgs::Odometry>(
      best_estimate, 1000);
  ros::Rate loop_rate(10);
  while (ros::ok()) {
    if (publishing_flag) {
      nav_msgs::Odometry odometry_data(localization->BestEstimate());
      tf::Transform transform;
      transform.setOrigin(tf::Vector3(odometry_data.pose.pose.position.x,
                                      odometry_data.pose.pose.position.y,
                                      odometry_data.pose.pose.position.z));
      transform.setRotation(tf::Quaternion(
          odometry_data.pose.pose.orientation.x,
          odometry_data.pose.pose.orientation.y,
          odometry_data.pose.pose.orientation.z,
          odometry_data.pose.pose.orientation.w));
      tb.sendTransform(tf::StampedTransform(
        transform, ros::Time::now(), map_frame_id, frame_id));
      candidates_pub.publish(localization->SolutionPoints());
      best_estimate_pub.publish(odometry_data);
      publishing_flag = false;
    }
    ros::spinOnce();
    loop_rate.sleep();
  }
}
